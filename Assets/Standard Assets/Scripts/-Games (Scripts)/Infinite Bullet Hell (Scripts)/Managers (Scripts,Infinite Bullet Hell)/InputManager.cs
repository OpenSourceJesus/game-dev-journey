using UnityEngine;
using Extensions;
using System;
using System.Collections.Generic;
// using UnityEngine.InputSystem;
using GameDevJourney;

namespace InfiniteBulletHell
{
	public class InputManager : GameDevJourney.InputManager
	{
		public bool useMouseForPlayerMovement;
		public new static InputManager instance;
		public new static InputManager Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<InputManager>();
				return instance;
			}
		}
		public static Vector2 MoveInput
		{
			get
			{
				return GetMoveInput(MathfExtensions.NULL_INT);
			}
		}
		public Vector2 _MoveInput
		{
			get
			{
				return MoveInput;
			}
		}
		public static bool SlowMovementInput
		{
			get
			{
				return GetSlowMovementInput(MathfExtensions.NULL_INT);
			}
		}
		public bool _SlowMovementInput
		{
			get
			{
				return SlowMovementInput;
			}
		}
		public static bool ItemMenuInput
		{
			get
			{
				return GetItemMenuInput(MathfExtensions.NULL_INT);
			}
		}
		public bool _ItemMenuInput
		{
			get
			{
				return ItemMenuInput;
			}
		}

		public static Vector2 GetMoveInput (int playerIndex)
		{
			Vector2 worldMousePosition = CameraScript.instance.camera.ScreenToWorldPoint(Input.mousePosition);
			Vector2 move = worldMousePosition - (Vector2) Player.instance.trs.position;
			float moveAmount = Player.instance.moveSpeed * Time.deltaTime;
			if (move.sqrMagnitude <= moveAmount * moveAmount)
			{
				Player.instance.trs.position = worldMousePosition;
				return Vector2.zero;
			}
			else
				return move.normalized;
			// if (UsingGamepad)
			// 	return Vector2.ClampMagnitude(GetGamepad(playerIndex).leftStick.ReadValue(), 1);
			// else
			// {
			// 	if (Instance.useMouseForPlayerMovement)
			// 	{
			// 		Vector2 mousePosition = GetMouse(playerIndex).position.ReadValue();
			// 		// return Vector2.ClampMagnitude(CameraScript.instance.camera.ScreenToWorldPoint(mousePosition) - Player.instance.trs.position, 1);
			// 		Vector2 worldMousePosition = CameraScript.instance.camera.ScreenToWorldPoint(mousePosition);
			// 		Vector2 move = worldMousePosition - (Vector2) Player.instance.trs.position;
			// 		float moveAmount = Player.instance.moveSpeed * Time.deltaTime;
			// 		if (move.sqrMagnitude <= moveAmount * moveAmount)
			// 		{
			// 			Player.instance.trs.position = worldMousePosition;
			// 			return Vector2.zero;
			// 		}
			// 		else
			// 			return move.normalized;
			// 	}
			// 	Keyboard keyboard = GetKeyboard(playerIndex);
			// 	int x = 0;
			// 	if (keyboard.dKey.isPressed)
			// 		x ++;
			// 	if (keyboard.aKey.isPressed)
			// 		x --;
			// 	if (x == 0)
			// 	{
			// 		if (keyboard.leftArrowKey.isPressed)
			// 			x --;
			// 		if (keyboard.rightArrowKey.isPressed)
			// 			x ++;
			// 	}
			// 	int y = 0;
			// 	if (keyboard.wKey.isPressed)
			// 		y ++;
			// 	if (keyboard.sKey.isPressed)
			// 		y --;
			// 	if (y == 0)
			// 	{
			// 		if (keyboard.downArrowKey.isPressed)
			// 			y --;
			// 		if (keyboard.upArrowKey.isPressed)
			// 			y ++;
			// 	}
			// 	return Vector2.ClampMagnitude(new Vector2(x, y), 1);
			// }
		}

		public static bool GetSlowMovementInput (int playerIndex)
		{
			// if (UsingGamepad)
			// 	return GetGamepad(playerIndex).rightTrigger.isPressed;
			// else
			// {
			// 	Keyboard keyboard = GetKeyboard(playerIndex);
			// 	return keyboard.shiftKey.isPressed || keyboard.tabKey.isPressed || keyboard.leftCtrlKey.isPressed || keyboard.rightCtrlKey.isPressed || GetMouse(playerIndex).leftButton.isPressed;
			// }
			return Input.GetMouseButton(0);
		}

		public static bool GetItemMenuInput (int playerIndex)
		{
			// if (UsingGamepad)
			// 	return GetGamepad(playerIndex).selectButton.isPressed;
			// else
			// 	return GetKeyboard(playerIndex).spaceKey.isPressed || GetMouse(playerIndex).rightButton.isPressed;
			return Input.GetMouseButton(1);
		}
	}
}