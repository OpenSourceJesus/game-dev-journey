﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Object = UnityEngine.Object;
using GameDevJourney;

namespace InfiniteBulletHell
{
	public class GameManager : GameDevJourney.GameManager
	{
		public new static GameManager instance;
		public new static GameManager Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<GameManager>();
				return instance;
			}
		}
		public Transform trs;
		public Room[] roomPrefabs = new Room[0];
		public EnemySpawnEntry[] enemySpawnEntries;
		public ItemSpawnEntry[] itemSpawnEntries;
		public static List<Enemy> enemies = new List<Enemy>();
		public float difficulty;
		public float minSpawnDistFromPlayer;
		[HideInInspector]
		public float minSpawnDistFromPlayerSqr;
		public float increaseDifficultyAmount;
		public static List<Bullet> bullets = new List<Bullet>();
		public Text bulletCountText;
		public static float score;
		public Text scoreText;
		public Text bestScoreText;
		public static Dictionary<Vector2Int, Room> rooms = new Dictionary<Vector2Int, Room>();
		public Text frameRateText;
		public int preloadOpenExitCount;
		public static Room currentRoom;
		public float bulletSpeedModifier;
		public float BulletSpeedMultiplier
		{
			get
			{
				float output = 1;
				if (bulletSpeedModifier < 0)
					output /= 1f - bulletSpeedModifier;
				else
					output += bulletSpeedModifier;
				return output;
			}
		}
		public float itemCostPerDifficultyFactor;
		public float itemFrequency;
		public EffectTimerVisualizer effectTimerVisualizerPrefab;
		public Transform effectTimerVisualizersParent;
		int totalItemValue;
		int currentDifficulty;
		bool itemMenuInput;
		bool previousItemMenuInput;
		
		void Start ()
		{
			instance = this;
			enemies.Clear();
			bullets.Clear();
			rooms.Clear();
			currentRoom = null;
			Player.Instance.gameObject.SetActive(false);
			SetScore (0);
			bestScoreText.text = "Best score: " + PlayerPrefs.GetInt("Best Score", 0);
			StartCoroutine(RetryPreloadRooms ());
		}
		
		IEnumerator RetryPreloadRooms ()
		{
			yield return StartCoroutine(MakeNewRoom (Vector2Int.zero));
			yield return StartCoroutine(PreloadRooms ());
			while (GetOpenExits().Count < preloadOpenExitCount && GetOpenExits().Count > 0)
				yield return new WaitForEndOfFrame();
			if (GetOpenExits().Count == 0)
			{
				// print("Retrying to preload rooms");
				// yield return StartCoroutine(RetryPreloadRooms ());
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}
			else
				Init ();
		}
		
		void Init ()
		{
			StopAllCoroutines();
			foreach (Room room in rooms.Values)
				room.gameObject.SetActive(false);
			currentRoom = rooms[Vector2Int.zero];
			Player.instance.trs.position = currentRoom.playerStartLocations[Random.Range(0, currentRoom.playerStartLocations.Length)].position;
			Player.instance.gameObject.SetActive(true);
			currentRoom.gameObject.SetActive(true);
			SpawnRoomObjects ();
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			minSpawnDistFromPlayerSqr = minSpawnDistFromPlayer * minSpawnDistFromPlayer;
		}
#endif
		
		IEnumerator PreloadRooms ()
		{
			while (GetOpenExits().Count < preloadOpenExitCount && GetOpenExits().Count > 0)
			{
				Transform openExit = GetOpenExits()[0];
				yield return StartCoroutine(MakeNewRoom (openExit));
				// yield return new WaitForEndOfFrame();
			}
		}
		
		List<Transform> GetOpenExits ()
		{
			List<Transform> output = new List<Transform>();
			foreach (Room room in rooms.Values)
			{
				foreach (Transform exit in room.exits)
				{
					if (!rooms.ContainsKey(room.location + exit.position.normalized.ToVec2Int()))
						output.Add(exit);
				}
			}
			return output;
		}
		
		List<Room> GetOpenRooms ()
		{
			List<Room> output = new List<Room>();
			foreach (Room room in rooms.Values)
			{
				foreach (Transform exit in room.exits)
				{
					if (!rooms.ContainsKey(room.location + exit.position.normalized.ToVec2Int()))
					{
						output.Add(room);
						break;
					}
				}
			}
			return output;
		}
		
		public override void Update ()
		{
			itemMenuInput = InputManager.ItemMenuInput;
			//for (int i = 0; i < bullets.Count; i ++)
			//{
			//	Bullet bullet = bullets[i];
			//	bullet.trs.position += bullet.trs.up * bullet.speed * Time.deltaTime * BulletSpeedMultiplier;
			//	if (!bullet.collider.bounds.Intersects(currentRoom.bounds))
			//	{
			//		GameManager.GetSingleton<ObjectPool>().Despawn(bullet.gameObject, bullet.trs);
			//		i --;
			//	}
			//}
			base.Update ();
			// bulletCountText.text = "" + bullets.Count;
			// frameRateText.text = "" + MathfExtensions.SnapToInterval(1f / Time.deltaTime, 1f);
			HandleItemMenu ();
			previousItemMenuInput = itemMenuInput;
		}

		void HandleItemMenu ()
		{
			if (itemMenuInput && !previousItemMenuInput)
			{
				if (!ItemMenu.isOpen)
					ItemMenu.instance.Open ();
				else
					ItemMenu.instance.Close ();
			}
		}
		
		public IEnumerator MakeNewRoom (Transform exit)
		{
			Vector2Int roomLocation = exit.parent.GetComponent<Room>().location + exit.position.normalized.ToVec2Int();
			yield return StartCoroutine(MakeNewRoom(roomLocation));
		}
		
		public IEnumerator MakeNewRoom (Vector2Int roomLocation)
		{
			Room output = null;
			Room nextRoom = null;
			float roomRotation = 0;
			if (!rooms.ContainsKey(roomLocation))
			{
				while (output == null)
				{
					nextRoom = roomPrefabs[Random.Range(0, roomPrefabs.Length)];
					roomRotation = Random.Range(0, 4) * 90;
					output = Instantiate(nextRoom, Vector2.zero, Quaternion.Euler(Vector3.forward * roomRotation));
					output.location = roomLocation;
					// yield return null;
					if (rooms.Count > 0 && !CheckIfRoomIsValid(output))
						DestroyImmediate(output.gameObject);
					// yield return new WaitForEndOfFrame();
				}
			}
			if (output != null)
			{
				rooms.Add(roomLocation, output);
				//if (Player.instance.currentRoomLocation == roomLocation)
				//	output.explored = true;
				currentRoom = output;
			}
			yield break;
		}
		
		public void CleanupCurrentRoom ()
		{
			currentRoom.gameObject.SetActive(false);
			while (enemies.Count > 0)
				Destroy(enemies[0].gameObject);
			ClearBullets ();
			currentDifficulty = 0;
			// totalItemValue = 0;
		}
		
		public void ClearCurrentRoom ()
		{
			currentRoom.itemsParentGo.SetActive(true);
			if (currentRoom.enemyStartPositions.Count > 0)
				AddScore ();
			currentRoom.enemyStartPositions.Clear ();
			foreach (Transform exit in currentRoom.exits)
				exit.gameObject.SetActive(false);
		}
		
		public void IncreaseDifficulty ()
		{
			difficulty += increaseDifficultyAmount;
		}
		
		bool CheckIfRoomIsValid (Room room)
		{
			bool output = false;
			if (rooms.ContainsKey(room.location + Vector2Int.up))
				output |= CheckIfRoomConnectionIsValid(room, rooms[room.location + Vector2Int.up]);
			if (!output && rooms.ContainsKey(room.location + Vector2Int.down))
				output |= CheckIfRoomConnectionIsValid(room, rooms[room.location + Vector2Int.down]);
			if (!output && rooms.ContainsKey(room.location + Vector2Int.right))
				output |= CheckIfRoomConnectionIsValid(room, rooms[room.location + Vector2Int.right]);
			if (!output && rooms.ContainsKey(room.location + Vector2Int.left))
				output |= CheckIfRoomConnectionIsValid(room, rooms[room.location + Vector2Int.left]);
			return output;
		}
		
		bool CheckIfRoomConnectionIsValid (Room room1, Room room2)
		{
			bool output = false;
			Vector2Int toRoom2 = room2.location - room1.location;
			bool room1Connects = false;
			foreach (Transform exit1 in room1.exits)
			{
				if (exit1.position.normalized.ToVec2Int() == toRoom2)
				{
					room1Connects = true;
					break;
				}
			}
			bool room2Connects = false;
			foreach (Transform exit2 in room2.exits)
			{
				if (exit2.position.normalized.ToVec2Int() == -toRoom2)
				{
					room2Connects = true;
					break;
				}
			}
			output = room1Connects && room2Connects;
			return output;
		}
		
		public void SpawnRoomObjects ()
		{
			if (!currentRoom.explored)
			{
				SpawnEnemies ();
				if (itemFrequency != Mathf.Infinity && difficulty % itemFrequency == 0)
					SpawnItems ();
			}
			currentRoom.explored = true;
		}
		
		void SpawnEnemies ()
		{
			List<EnemySpawnEntry> _enemySpawnEntries = new List<EnemySpawnEntry>(enemySpawnEntries);
			do
			{
				int randomIndex = Random.Range(0, _enemySpawnEntries.Count);
				EnemySpawnEntry enemySpawnEntry = _enemySpawnEntries[randomIndex];
				if (currentDifficulty + enemySpawnEntry.enemyPrefab.difficulty <= (int) difficulty)
				{
					if (Random.value < enemySpawnEntry.chance)
						SpawnEnemy (enemySpawnEntry.enemyPrefab);
				}
				else
					_enemySpawnEntries.RemoveAt(randomIndex);
			}
			while (_enemySpawnEntries.Count > 0);
			currentRoom.SpawnEnemies ();
		}
		
		void SpawnEnemy (Enemy enemyPrefab)
		{
			Vector2 mapMin = currentRoom.bounds.min.Snap(Vector3.one);
			Vector2 mapMax = currentRoom.bounds.max.Snap(Vector3.one);
			Vector2 spawnPos;
			do
			{
				spawnPos = new Vector2(Random.Range(mapMin.x, mapMax.x), Random.Range(mapMin.y, mapMax.y));
			} while (Physics2D.OverlapPoint(spawnPos, Physics2D.GetLayerCollisionMask(enemyPrefab.gameObject.layer)) != null || (spawnPos - (Vector2) Player.instance.trs.position).sqrMagnitude < minSpawnDistFromPlayerSqr);
			//Enemy newEnemy = Instantiate(enemyPrefab, spawnPos, enemyPrefab.trs.rotation);
			currentRoom.enemyStartPositions.Add(spawnPos, enemyPrefab);
			currentDifficulty += enemyPrefab.difficulty;
		}
		
		void SpawnItems ()
		{
			// print(totalItemValue);
			// totalItemValue = 0;
			int totalRemainderCost = 0;
			List<ItemSpawnEntry> _itemSpawnEntries = new List<ItemSpawnEntry>(itemSpawnEntries);
			do
			{
				int randomIndex = Random.Range(0, _itemSpawnEntries.Count);
				ItemSpawnEntry spawnItemEntry = _itemSpawnEntries[randomIndex];
				int remainderCost = (int) (difficulty * itemCostPerDifficultyFactor) - (totalItemValue + spawnItemEntry.itemPrefab.cost);
				if (remainderCost >= 0)
				{
					if (Random.value < spawnItemEntry.chance)
						SpawnItem (spawnItemEntry.itemPrefab);
				}
				else
				{
					_itemSpawnEntries.RemoveAt(randomIndex);
				}
			}
			while (_itemSpawnEntries.Count > 0);
			totalItemValue -= (int) (difficulty * itemCostPerDifficultyFactor);
		}
		
		void SpawnItem (Item itemPrefab)
		{
			Vector2 mapMin = currentRoom.bounds.min.Snap(Vector3.one);
			Vector2 mapMax = currentRoom.bounds.max.Snap(Vector3.one);
			Vector2 spawnPos;
			do
			{
				spawnPos = new Vector2(Random.Range(mapMin.x, mapMax.x), Random.Range(mapMin.y, mapMax.y));
			} while (Physics2D.OverlapPoint(spawnPos, LayerMask.GetMask("Wall")) != null);
			Instantiate(itemPrefab, spawnPos, itemPrefab.trs.rotation, currentRoom.itemsParentGo.transform);
			totalItemValue += itemPrefab.cost;
		}
		
		public void LoadPreviousRoom (Room room)
		{
			currentRoom = room;
			room.gameObject.SetActive(true);
			if (!room.explored)
				IncreaseDifficulty ();
			SpawnRoomObjects ();
		}
		
		public void ClearBullets ()
		{
			while (bullets.Count > 0)
			{
				Bullet bullet = bullets[0];
				ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
			}
		}
		
		public void GameOver ()
		{
			// CleanupCurrentRoom ();
			// for (int i = 0; i < bullets.Count; i ++)
			// 	Destroy(bullets[i].gameObject);
			if ((int) score > PlayerPrefs.GetInt("Best Score", 0))
				PlayerPrefs.SetInt("Best Score", (int) score);
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
		
		public void SetScore (float amount)
		{
			score = amount;
			scoreText.text = "Score: " + (int) score;
		}
		
		public void AddScore (float amount = 1)
		{
			SetScore (score + amount);
		}
		
		[Serializable]
		public class EnemySpawnEntry : SpawnEntry
		{
			public Enemy enemyPrefab;
			public static EnemySpawnEntry nextEnemyEntry;
		}
		
		[Serializable]
		public class ItemSpawnEntry : SpawnEntry
		{
			public Item itemPrefab;
			public static ItemSpawnEntry nextItemEntry;
		}
		
		public class SpawnEntry
		{
			public GameObject prefab;
			[Range(0, 1)]
			public float chance;
			public static SpawnEntry nextEntry;
		}
	}
}