﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevJourney;

namespace InfiniteBulletHell
{
	public class Destructable : MonoBehaviour
	{
		public float maxHp;
		[HideInInspector]
		public float hp;
		public Animation anim;
		public AnimationClip getHurtClip;
		public AnimationClip deathClip;
		
		public virtual void Start ()
		{
			hp = maxHp;
		}
		
		public virtual void TakeDamage (float damage)
		{
			if (anim != null)
			{
				if (anim.IsPlaying(getHurtClip.name))
					return;
				anim.Play(getHurtClip.name);
			}
			hp -= damage;
			if (hp <= 0)
				Death ();
		}
		
		public virtual void Death ()
		{
			if (anim != null)
				anim.Play(deathClip.name);
			Destroy(gameObject);
		}
	}
}