﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using UnityEngine.UI;
using GameDevJourney;
using GameManager = InfiniteBulletHell.GameManager;

namespace InfiniteBulletHell
{
	public class Player : Shooter, IEffectable, ISaveableAndLoadable
	{
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int uniqueId;
		public int UniqueId
		{
			get
			{
				return uniqueId;
			}
			set
			{
				uniqueId = value;
			}
		}
		public Collider2D collider;
		public Collider2D enemyBulletSensor;
		Collider2D hitCollider;
		[HideInInspector]
		[SaveAndLoadValue(false)]
		public Vector2Int currentRoomLocation;
		public LineRenderer enemyLocaterPrefab;
		List<LineRenderer> enemyLocaters = new List<LineRenderer>();
		public Text hpText;
		[SaveAndLoadValue(false)]
		public float damage;
		public float defaultMoveSpeed;
		public float slowMoveSpeedDenominator;
		[HideInInspector]
		public int thingsMakingMeInvulnerable;
		
		public override void Start ()
		{
			instance = this;
			base.Start ();
			hpText.text = "Health: " + hp;
			moveSpeed = defaultMoveSpeed;
		}
		
		public override void DoUpdate ()
		{
			base.DoUpdate ();
			// HandleCollision ();
			HandleMovementSpeed ();
			Move (InputManager.MoveInput);
			//HandleEnemyLocaters ();
			HandleAttack ();
			HandleLeavingRoom ();
		}
		
		void HandleMovementSpeed ()
		{
			if (InputManager.SlowMovementInput)
				moveSpeed = defaultMoveSpeed / slowMoveSpeedDenominator;
			else
				moveSpeed = defaultMoveSpeed;
		}
		
		void HandleAttack ()
		{
			for (int i = 0; i < GameManager.enemies.Count; i ++)
			{
				Enemy enemy = GameManager.enemies[i];
				enemy.TakeDamage (Time.deltaTime * damage * (1f / Vector2.Distance(trs.position, enemy.trs.position)));
			}
		}
		
		void HandleLeavingRoom ()
		{
			if (!collider.bounds.Intersects(GameManager.currentRoom.bounds))
			{
				if (trs.position.x < GameManager.currentRoom.bounds.min.x)
					LeaveRoom (Vector2Int.left);
				else if (trs.position.x > GameManager.currentRoom.bounds.max.x)
					LeaveRoom (Vector2Int.right);
				else if (trs.position.y < GameManager.currentRoom.bounds.min.y)
					LeaveRoom (Vector2Int.down);
				else
					LeaveRoom (Vector2Int.up);
			}
		}
		
		void HandleCollision ()
		{
			hitCollider = Physics2D.OverlapPoint(trs.position, LayerMask.GetMask("Enemy Bullet"));
			if (hitCollider != null)
			{
				Bullet bullet = hitCollider.GetComponent<Bullet>();
				if (bullet != null)
				{
					ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
					if ((anim != null && anim.IsPlaying(getHurtClip.name)) || thingsMakingMeInvulnerable > 0)
						return;
					TakeDamage (bullet.damage);
				}
			}
		}
		
		public override void TakeDamage (float damage)
		{
			base.TakeDamage (damage);
			hpText.text = "Health: " + hp;
		}
		
		void OnDisable ()
		{
			rigid.simulated = false;
		}
		
		void OnEnable ()
		{
			rigid.simulated = true;
		}
		
		void HandleEnemyLocaters ()
		{
			int currentIndex = 0;
			while (GameManager.enemies.Count > enemyLocaters.Count)
			{
				Enemy enemy = GameManager.enemies[currentIndex];
				if (enemy != null)
				{
					LineRenderer enemyLocater = Instantiate(enemyLocaterPrefab, trs, false);
					enemyLocaters.Add(enemyLocater);
				}
				currentIndex ++;
			}
			for (int i = 0; i < enemyLocaters.Count - GameManager.enemies.Count; i ++)
			{
				Destroy(enemyLocaters[i].gameObject);
				enemyLocaters.RemoveAt(i);
				i --;
			}
			for (int i = 0; i < enemyLocaters.Count; i ++)
			{
				enemyLocaters[i].SetPosition(1, (GameManager.enemies[i].trs.position - Player.instance.trs.position) * (1f / trs.localScale.x));
				enemyLocaters[i].startColor = GameManager.enemies[i].spriteRenderer.color.SetAlpha(enemyLocaters[i].startColor.a);
				enemyLocaters[i].endColor = GameManager.enemies[i].spriteRenderer.color.SetAlpha(enemyLocaters[i].endColor.a);
			}
		}
		
		void LeaveRoom (Vector2Int directionToNextRoom)
		{
			if (!enabled)
				return;
			enabled = false;
			StartCoroutine(LeaveRoomRoutine (directionToNextRoom));
		}
		
		IEnumerator LeaveRoomRoutine (Vector2Int directionToNextRoom)
		{
			trs.position -= (directionToNextRoom * (int) GameManager.currentRoom.bounds.size.x).ToVec3();
			currentRoomLocation += directionToNextRoom;
			GameManager.instance.CleanupCurrentRoom ();
			if (!GameManager.rooms.ContainsKey(currentRoomLocation))
			{
				GameManager.instance.IncreaseDifficulty ();
				yield return GameManager.instance.StartCoroutine(GameManager.instance.MakeNewRoom (currentRoomLocation));
				GameManager.instance.SpawnRoomObjects ();
			}
			else
				GameManager.instance.LoadPreviousRoom (GameManager.rooms[currentRoomLocation]);
			enabled = true;
		}
		
		public override void Death ()
		{
			if (!enabled)
				return;
			enabled = false;
			GameManager.instance.GameOver ();
		}
	}
}