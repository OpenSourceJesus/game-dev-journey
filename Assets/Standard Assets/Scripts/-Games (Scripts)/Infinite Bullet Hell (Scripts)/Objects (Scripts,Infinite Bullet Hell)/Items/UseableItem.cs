﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class UseableItem : Item
	{
		[SaveAndLoadValue(false)]
		public int usedCount;
		public int uses;

		public virtual void Use ()
		{
			usedCount ++;
			if (usedCount == uses)
				Destroy(gameObject);
		}
	}
}