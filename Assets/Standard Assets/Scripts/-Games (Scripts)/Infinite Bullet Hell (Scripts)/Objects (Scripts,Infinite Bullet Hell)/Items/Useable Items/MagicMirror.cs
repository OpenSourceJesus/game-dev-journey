﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class MagicMirror : UseableDurationalItem
	{
		public override void Use ()
		{
			Player.instance.trs.position *= -1;
			base.Use ();
		}

		public override IEnumerator UseRoutine ()
		{
			Player.instance.thingsMakingMeInvulnerable ++;
			yield return GameManager.instance.StartCoroutine(base.UseRoutine ());
			Player.instance.thingsMakingMeInvulnerable --;
		}
	}
}