﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class Heart : UseableItem
	{
		public override void Use ()
		{
			Player.instance.maxHp ++;
			Player.instance.hp ++;
			Player.instance.hpText.text = "Health: " + Player.instance.hp;
			base.Use ();
		}
	}
}