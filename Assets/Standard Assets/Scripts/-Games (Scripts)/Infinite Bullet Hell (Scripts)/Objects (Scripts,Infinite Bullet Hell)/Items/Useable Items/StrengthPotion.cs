﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class StrengthPotion : UseableDurationalItem
	{
		public float multiplyDamage;

		public override IEnumerator UseRoutine ()
		{
			Player.instance.damage *= multiplyDamage;
			yield return GameManager.instance.StartCoroutine(base.UseRoutine ());
			Player.instance.damage /= multiplyDamage;
		}
	}
}