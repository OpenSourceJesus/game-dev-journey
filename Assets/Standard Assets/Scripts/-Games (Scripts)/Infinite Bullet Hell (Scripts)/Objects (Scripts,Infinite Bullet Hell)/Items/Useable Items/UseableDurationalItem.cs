﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class UseableDurationalItem : UseableItem
	{
		public float duration;

		public override void Use ()
		{
			EffectTimerVisualizer effectTimerVisualizer = Instantiate(GameManager.instance.effectTimerVisualizerPrefab, GameManager.instance.effectTimerVisualizersParent);
			effectTimerVisualizer.image.sprite = spriteRenderer.sprite;
			effectTimerVisualizer.timer.duration = duration;
			effectTimerVisualizer.Init ();
			GameManager.instance.StartCoroutine(UseRoutine ());
			base.Use ();
		}

		public virtual IEnumerator UseRoutine ()
		{
			yield return new WaitForSeconds(duration);
		}
	}
}