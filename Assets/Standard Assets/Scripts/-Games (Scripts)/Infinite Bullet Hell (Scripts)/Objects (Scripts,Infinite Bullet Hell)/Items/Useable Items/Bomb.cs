﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class Bomb : UseableItem
	{
		public override void Use ()
		{
			for (int i = 0; i < GameManager.bullets.Count; i ++)
			{
				Bullet bullet = GameManager.bullets[i];
				bullet.Despawn ();
				i --;
			}
			base.Use ();
		}
	}
}