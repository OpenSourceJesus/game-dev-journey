﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using GameDevJourney;

namespace InfiniteBulletHell
{
	public class Item : UpdateWhileEnabled, ISaveableAndLoadable
	{
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int UniqueId
		{
			get
			{
				return uniqueId;
			}
			set
			{
				uniqueId = value;
			}
		}
		public int uniqueId = MathfExtensions.NULL_INT;
		public int cost;
		public Tooltip tooltip;
		public Transform trs;
		public SpriteRenderer spriteRenderer;
		bool walkedOver;
		
		public override void DoUpdate ()
		{
			if (walkedOver)
				Pickup ();
		}
		
		public virtual void Pickup ()
		{
			ItemMenu.instance.AddEntry (this);
			gameObject.SetActive(false);
		}
		
		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			walkedOver = true;
			tooltip.gameObject.SetActive(true);
		}
		
		public virtual void OnTriggerExit2D (Collider2D other)
		{
			walkedOver = false;
			tooltip.gameObject.SetActive(false);
		}
	}
}