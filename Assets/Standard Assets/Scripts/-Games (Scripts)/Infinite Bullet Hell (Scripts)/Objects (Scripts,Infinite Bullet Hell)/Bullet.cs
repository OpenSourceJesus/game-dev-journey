﻿using UnityEngine;
using Extensions;
using GameDevJourney;

namespace InfiniteBulletHell
{
	public class Bullet : GameDevJourney.Bullet
	{
		public float damage;
		// [HideInInspector]
		// public float defaultSpeed;
		// [HideInInspector]
		// public Shooter shooter;
		
		public override void OnEnable ()
		{
		// 	defaultSpeed = speed;
			GameManager.bullets.Add(this);
			base.OnEnable ();
		}
		
		public override void OnDisable ()
		{
			GameManager.bullets.Remove(this);
		// 	shooter = null;
			base.OnDisable ();
		}
		
		// // public virtual void DoUpdate ()
		// // {
		// // 	deltaTime = Time.time - previousTime;
		// // 	trs.position += trs.up * speed * deltaTime * GameManager.instance.BulletSpeedMultiplier;
		// // 	if (!collider.bounds.Intersects(Room.instance.bounds))
		// // 		ObjectPool.instance.Despawn(prefabIndex, gameObject, trs);
		// // 	previousTime = Time.time;
		// // }
		
		public override void OnTriggerEnter2D (Collider2D other)
		{
			base.OnTriggerEnter2D (other);
			if (other == Player.instance.enemyBulletSensor && (Player.instance.anim == null || !Player.instance.anim.IsPlaying(Player.instance.getHurtClip.name)))
				Player.instance.TakeDamage (damage);
			// Effector effector = other.GetComponent<Effector>();
			// if (effector != null)
			// 	effector.OnTriggerEnter2D (collider);
		}
		
		// // public virtual void OnTriggerExit2D (Collider2D other)
		// // {
		// // 	Effector effector = other.GetComponent<Effector>();
		// // 	if (effector != null)
		// // 		effector.OnTriggerExit2D (collider);
		// // }
	}
}