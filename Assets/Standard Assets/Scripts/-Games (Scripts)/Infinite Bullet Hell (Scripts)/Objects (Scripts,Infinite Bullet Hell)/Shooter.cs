﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System;
using GameDevJourney;

namespace InfiniteBulletHell
{
	public class Shooter : Destructable, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public Transform trs;
		public float moveSpeed;
		public Rigidbody2D rigid;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		float move;
		
		public override void Start ()
		{
			base.Start ();
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntry.bulletPattern.Init (bulletPatternEntry.spawner);
			}
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public virtual void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
		
		public virtual void Move (Vector2 move)
		{
			rigid.velocity = move * moveSpeed;
		}
		
		public virtual void RotateTo (Vector2 newUpDir)
		{
			trs.up = newUpDir;
		}
		
		public virtual void DoUpdate ()
		{
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntry.reloadTimer -= Time.deltaTime;
				if (bulletPatternEntry.reloadTimer < 0)
				{
					bulletPatternEntry.reloadTimer = bulletPatternEntry.reloadTime;
					GameDevJourney.Bullet[] bullets = bulletPatternEntry.bulletPattern.Shoot(bulletPatternEntry.spawner, bulletPatternEntry.bulletPrefab);
					// for (int i = 0; i < bullets.Length; i ++)
					// {
					// 	Bullet bullet = bullets[i] as Bullet;
					// 	bullet.shooter = this;
					// }
				}
			}
		}
		
		[Serializable]
		public class BulletPatternEntry
		{
			public BulletPattern bulletPattern;
			public Transform spawner;
			public Bullet bulletPrefab;
			public float reloadTime;
			[HideInInspector]
			public float reloadTimer;
		}
	}
}