﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class SpeedEffector : Effector
	{
		public float speedMultiplier;
		
		public override void OnTriggerEnter2D (Collider2D other)
		{
			Player.instance.defaultMoveSpeed *= speedMultiplier;
			// IEffectable effectable = other.GetComponent<IEffectable>();
			// if (effectable != null)
			// {
			// 	Player player = effectable as Player;
			// 	if (player != null)
			// 		player.defaultMoveSpeed *= speedMultiplier;
			// 	else
			// 	{
			// 		Shooter shooter =  effectable as Shooter;
			// 		if (shooter != null)
			// 			shooter.moveSpeed *= speedMultiplier; 
			// 		else
			// 		{
			// 			Bullet bullet = effectable as Bullet;
			// 			if (bullet != null)
			// 				bullet.speed = bullet.defaultSpeed * speedMultiplier;
			// 		}
			// 	}
			// }
		}
		
		public override void OnTriggerExit2D (Collider2D other)
		{
			Player.instance.defaultMoveSpeed /= speedMultiplier;
			// IEffectable effectable = other.GetComponent<IEffectable>();
			// if (effectable != null)
			// {
			// 	Player player = effectable as Player;
			// 	if (player != null)
			// 		player.defaultMoveSpeed /= speedMultiplier;
			// 	else
			// 	{
			// 		Shooter shooter = effectable as Shooter;
			// 		if (shooter != null)
			// 			shooter.moveSpeed /= speedMultiplier; 
			// 		else
			// 		{
			// 			Bullet bullet = effectable as Bullet;
			// 			if (bullet != null)
			// 				bullet.speed = bullet.defaultSpeed;
			// 		}
			// 	}
			// }
		}
	}
}