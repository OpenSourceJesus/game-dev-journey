﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevJourney;

namespace InfiniteBulletHell
{
	public class Effector : PhysicsObject2D
	{
		public Transform trs;
		public Rigidbody2D rigid;
		
		public virtual void OnTriggerEnter2D (Collider2D other)
		{
		}
		
		public virtual void OnTriggerExit2D (Collider2D other)
		{
			
		}
	}
}