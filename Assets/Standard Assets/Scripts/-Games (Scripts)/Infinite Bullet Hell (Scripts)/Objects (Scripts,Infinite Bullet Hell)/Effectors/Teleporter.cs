﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	public class Teleporter : Effector
	{
		public override void OnTriggerEnter2D (Collider2D other)
		{
			IEffectable effectable = other.GetComponent<IEffectable>();
			if (effectable != null)
			{
				Player player = effectable as Player;
				if (player != null)
				{
					player.trs.position += (trs.position - player.trs.position).normalized * trs.lossyScale.x;
					Destroy(gameObject);
				}
			}
		}
	}
}