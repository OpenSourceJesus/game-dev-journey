﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using GameDevJourney;

namespace InfiniteBulletHell
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class Enemy : Shooter
	{
		public int difficulty;
		// public LayerMask whatHurtsMe;
		public SpriteRenderer spriteRenderer;
		public Gradient hpFractionToColorGradient;
		
		public virtual void OnEnable ()
		{
			GameManager.enemies.Add(this);
		}
		
		public virtual void OnDisable ()
		{
			GameManager.enemies.Remove(this);
			hp = maxHp;
			spriteRenderer.color = hpFractionToColorGradient.Evaluate(1);
		}
		
		public override void Death ()
		{
			GameManager.enemies.Remove(this);
			if (GameManager.enemies.Count == 0)
				GameManager.instance.ClearCurrentRoom ();
			base.Death ();
		}
		
		public override void Start ()
		{
			base.Start ();
			GradientColorKey[] colorKeys = hpFractionToColorGradient.colorKeys;
			colorKeys[colorKeys.Length - 1].color = spriteRenderer.color;
			GradientAlphaKey[] alphaKeys = hpFractionToColorGradient.alphaKeys;
			hpFractionToColorGradient.SetKeys(colorKeys, alphaKeys);
		}
		
		public override void DoUpdate ()
		{
			base.DoUpdate ();
			Move ((Player.instance.trs.position - (Vector3) rigid.position).normalized);
		}
		
		// public virtual void OnTriggerEnter2D (Collider2D other)
		// {
		// 	if (LayerMaskExtensions.MaskContainsLayer(whatHurtsMe, other.gameObject.layer))
		// 	{
		// 		Bullet bullet = other.GetComponent<Bullet>();
		// 		if (bullet != null && bullet.shooter != null && bullet.shooter != this)
		// 		{
		// 			ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
		// 			TakeDamage (bullet.damage);
		// 		}
		// 	}
		// }
		
		public override void TakeDamage (float damage)
		{
			base.TakeDamage (damage);
			spriteRenderer.color = hpFractionToColorGradient.Evaluate(hp / maxHp);
		}
	}
}