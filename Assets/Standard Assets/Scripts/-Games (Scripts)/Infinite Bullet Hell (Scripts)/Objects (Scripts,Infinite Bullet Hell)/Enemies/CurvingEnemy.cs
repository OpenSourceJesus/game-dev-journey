﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace InfiniteBulletHell
{
	public class CurvingEnemy : Enemy
	{
		public float turnRate;
		
		public override void Start ()
		{
			base.Start ();
			trs.up = Player.instance.trs.position - trs.position;
		}
		
		public override void DoUpdate ()
		{
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntry.reloadTimer -= Time.deltaTime;
				if (bulletPatternEntry.reloadTimer < 0)
				{
					bulletPatternEntry.reloadTimer = bulletPatternEntry.reloadTime;
					GameDevJourney.Bullet[] bullets = bulletPatternEntry.bulletPattern.Shoot(bulletPatternEntry.spawner, bulletPatternEntry.bulletPrefab);
					// for (int i = 0; i < bullets.Length; i ++)
					// {
					// 	Bullet bullet = bullets[i] as Bullet;
					// 	bullet.shooter = this;
					// }
				}
			}
			RotateTo (VectorExtensions.RotateTo(trs.up, Player.instance.trs.position - trs.position, turnRate * Time.deltaTime));
			Move (trs.up);
		}
	}
}