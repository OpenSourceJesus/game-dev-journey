﻿using System.Collections.Generic;
using UnityEngine;
using Extensions;
using GameDevJourney;
using UnityEngine.UI;

namespace InfiniteBulletHell
{
	[ExecuteInEditMode]
	public class Room : SingletonMonoBehaviour<Room>
	{
		[HideInInspector]
		public Vector2Int location;
		public Transform trs;
		public Transform[] playerStartLocations = new Transform[0];
		// [HideInInspector]
		public Bounds bounds;
		public List<Transform> exits = new List<Transform>();
		[HideInInspector]
		public bool explored;
		public Dictionary<Vector2, Enemy> enemyStartPositions = new Dictionary<Vector2, Enemy>();
		public GameObject itemsParentGo;
		public Image backgroundImage;
		public Sprite[] backgroundSprites = new Sprite[0];
		
#if UNITY_EDITOR
		public bool autoSetBounds;

		void OnEnable ()
		{
			if (Application.isPlaying)
				return;
			if (autoSetBounds)
			{
				List<Bounds> levelBoundsInstances = new List<Bounds>();
				Renderer[] renderers = GetComponentsInChildren<Renderer>();
				for (int i = 0; i < renderers.Length; i ++)
				{
					Renderer renderer = renderers[i];
					if (renderer.GetComponent<OmitFromLevelMap>() == null)
						levelBoundsInstances.Add(renderer.bounds);
				}
				bounds = BoundsExtensions.Combine(levelBoundsInstances.ToArray());
			}
		}
#endif

		void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			backgroundImage.sprite = backgroundSprites[Random.Range(0, backgroundSprites.Length)];
		}
		
		public void SpawnEnemies ()
		{
			foreach (KeyValuePair<Vector2, Enemy> enemyAndStartPosition in enemyStartPositions)
				Instantiate(enemyAndStartPosition.Value, enemyAndStartPosition.Key, enemyAndStartPosition.Value.trs.rotation);
		}
	}
}