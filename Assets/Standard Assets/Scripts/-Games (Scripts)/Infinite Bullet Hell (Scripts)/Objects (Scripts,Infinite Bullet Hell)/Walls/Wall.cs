﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using GameDevJourney;

namespace InfiniteBulletHell
{
	[RequireComponent(typeof(LineRenderer))]
	[RequireComponent(typeof(EdgeCollider2D))]
	[ExecuteInEditMode]
	public class Wall : PhysicsObject2D, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		[Header("Wall")]
		public LineRenderer line;
		public new EdgeCollider2D edgeCollider;
		Vector2[] verticies;
		public Color color;
		public FrequencyType randomizeColorFrequency;
		
		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (line == null)
					line = GetComponent<LineRenderer>();
				if (edgeCollider == null)
					edgeCollider = GetComponent<EdgeCollider2D>();
				return;
			}
#endif
			GameManager.updatables = GameManager.updatables.Add(this);
		}
		
		public void DoUpdate ()
		{
			switch (randomizeColorFrequency)
			{
				case FrequencyType.Once:
					color = new Color(Random.value, Random.value, Random.value, color.a);
					randomizeColorFrequency = FrequencyType.Never;
					break;
				case FrequencyType.Always:
					color = new Color(Random.value, Random.value, Random.value, color.a);
					break;
			}
			line.startColor = color;
			line.endColor = color;
#if UNITY_EDITOR
			if (Application.isPlaying)
				return;
			line.positionCount = edgeCollider.pointCount;
			verticies = new Vector2[edgeCollider.pointCount];
			for (int i = 0; i < edgeCollider.pointCount; i ++)
			{
				verticies[i] = VectorExtensions.Snap(edgeCollider.points[i], Vector2.one);
				line.SetPosition(i, verticies[i]);
			}
			edgeCollider.points = verticies;
#endif
		}

		void OnValidate ()
		{
			DoUpdate ();
		}

		void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
		
		public enum FrequencyType
		{
			Never = 0,
			Once = 1,
			Always = 2
		}
	}
}