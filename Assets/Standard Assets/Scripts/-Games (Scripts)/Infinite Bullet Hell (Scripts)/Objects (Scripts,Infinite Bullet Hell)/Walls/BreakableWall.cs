﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteBulletHell
{
	[RequireComponent(typeof(LineRenderer))]
	public class BreakableWall : Wall
	{
		public float breakTime;
		
		void OnCollisionEnter2D (Collision2D coll)
		{
			Destroy(gameObject, this.breakTime);
			StartCoroutine(FadeSprite ());
		}
		
		IEnumerator FadeSprite ()
		{
			while (true)
			{
				this.line.startColor -= new Color(0, 0, 0, Time.deltaTime / this.breakTime);
				this.line.endColor -= new Color(0, 0, 0, Time.deltaTime / this.breakTime);
				yield return new WaitForSeconds(0);
			}
		}
	}
}