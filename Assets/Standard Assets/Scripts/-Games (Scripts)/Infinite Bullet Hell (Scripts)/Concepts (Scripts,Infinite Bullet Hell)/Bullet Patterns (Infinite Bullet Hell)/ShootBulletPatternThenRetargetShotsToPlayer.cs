﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevJourney;

namespace InfiniteBulletHell
{
	[CreateAssetMenu]
	public class ShootBulletPatternThenRetargetShotsToPlayer : BulletPattern
	{
		public BulletPattern bulletPattern;
		[MakeConfigurable]
		public float retargetTime;
		[MakeConfigurable]
		public bool lastRetarget;
		
		public override GameDevJourney.Bullet[] Shoot (Transform spawner, GameDevJourney.Bullet bulletPrefab)
		{
			GameDevJourney.Bullet[] output = bulletPattern.Shoot (spawner, bulletPrefab);
			foreach (GameDevJourney.Bullet bullet in output)
				bullet.StartCoroutine(RetargetAfterDelay (bullet, retargetTime, lastRetarget));
			return output;
		}
		
		public override Vector2 GetRetargetDirection (GameDevJourney.Bullet bullet)
		{
			return Player.instance.trs.position - bullet.trs.position;
		}
	}
}