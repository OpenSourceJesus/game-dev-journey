﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace InfiniteBulletHell
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithRandomOffset : ShootAtPlayer
	{
		[MakeConfigurable]
		public float randomShootOffsetRange;
		
		public override Vector2 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(Random.Range(-randomShootOffsetRange / 2, randomShootOffsetRange / 2));
		}
	}
}