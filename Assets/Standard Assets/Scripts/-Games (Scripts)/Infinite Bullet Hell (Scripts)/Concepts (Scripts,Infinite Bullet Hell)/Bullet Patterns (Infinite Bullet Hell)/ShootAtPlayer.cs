﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevJourney;

namespace InfiniteBulletHell
{
	[CreateAssetMenu]
	public class ShootAtPlayer : BulletPattern
	{
		public override Vector2 GetShootDirection (Transform spawner)
		{
			return Player.instance.trs.position - spawner.position;
		}
	}
}