﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using GameDevJourney;

namespace InfiniteBulletHell
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithOffsetThenContinouslySplitWhereFacingWithOffset : ShootAtPlayerWithOffset
	{
		[MakeConfigurable]
		public float splitOffset;
		public Bullet splitBulletPrefab;
		[MakeConfigurable]
		public float splitTime;
		
		public override GameDevJourney.Bullet[] Shoot (Transform spawner, GameDevJourney.Bullet bulletPrefab)
		{
			GameDevJourney.Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				GameDevJourney.Bullet bullet = output[i];
				GameManager.instance.StartCoroutine(Split (bullet, splitBulletPrefab, splitTime));
			}
			return output;
		}
		
		IEnumerator Split (GameDevJourney.Bullet bullet, GameDevJourney.Bullet splitBulletPrefab, float splitTime)
		{
			yield return new WaitForSeconds(splitTime);
			Shoot (bullet.trs.position, GetSplitDirection(bullet), splitBulletPrefab);
			if (bullet.isActiveAndEnabled)
				bullet.StartCoroutine(Split (bullet, splitBulletPrefab, splitTime));
		}
		
		public override Vector2 GetSplitDirection (GameDevJourney.Bullet bullet)
		{
			return bullet.trs.up.Rotate(splitOffset);
		}
	}
}