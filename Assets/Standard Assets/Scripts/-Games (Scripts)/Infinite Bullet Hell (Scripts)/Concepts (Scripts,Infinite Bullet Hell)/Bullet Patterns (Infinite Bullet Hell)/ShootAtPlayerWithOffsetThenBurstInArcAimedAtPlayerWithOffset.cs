﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using GameDevJourney;

namespace InfiniteBulletHell
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithOffsetThenBurstInArcAimedAtPlayerWithOffset : ShootAtPlayerWithOffset
	{
		[MakeConfigurable]
		public float burstOffset;
		public GameDevJourney.Bullet burstBulletPrefab;
		[MakeConfigurable]
		public float burstTime;
		[MakeConfigurable]
		public float burstArc;
		[MakeConfigurable]
		public float burstNumber;
		
		public override GameDevJourney.Bullet[] Shoot (Transform spawner, GameDevJourney.Bullet bulletPrefab)
		{
			GameDevJourney.Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				GameDevJourney.Bullet bullet = output[i];
				GameManager.instance.StartCoroutine(Burst (bullet, burstBulletPrefab, burstTime));
			}
			return output;
		}
		
		IEnumerator Burst (GameDevJourney.Bullet bullet, GameDevJourney.Bullet burstBulletPrefab, float splitTime)
		{
			yield return new WaitForSeconds(splitTime);
			if (!bullet.isActiveAndEnabled)
				yield break;
			//float bulletDirection = VectorExtensions.GetFacingAngleOfVector(direction);
			float bulletDirection = VectorExtensions.GetFacingAngle(Player.instance.trs.position - bullet.trs.position);
			for (float splitAngle = bulletDirection - burstArc / 2 + burstOffset; splitAngle < bulletDirection + burstArc / 2 + burstOffset; splitAngle += burstArc / burstNumber)
				Shoot (bullet.trs.position, VectorExtensions.FromFacingAngle(splitAngle), burstBulletPrefab);
			ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
		}
	}
}