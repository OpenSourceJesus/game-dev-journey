using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using TMPro;
using UnityEngine.UI;
using Extensions;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class Tooltip : MonoBehaviour, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public RectTransform rectTrs;
		public Canvas canvas;
		public RectTransform canvasRectTrs;
		public TMP_Text text;

		public void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			if (rectTrs == null)
				rectTrs = GetComponent<RectTransform>();
			if (text == null)
				text = GetComponent<TMP_Text>();
#endif
			if (text != null && canvas != null && canvasRectTrs != null)
				GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (text == null)
			{
				GameManager.updatables = GameManager.updatables.Remove(this);
				return;
			}
			text.gameObject.SetActive(UIControlManager.IsMousedOverRectTransform(rectTrs, canvas, canvasRectTrs));
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}