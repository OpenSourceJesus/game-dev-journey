using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using GameDevJourney;

[ExecuteInEditMode]
public class EffectTimerVisualizer : UpdateWhileEnabled
{
	public Image image;
	public Transform trs;
	public Timer timer;

#if UNITY_EDITOR
	void Start ()
	{
		if (trs == null)
			trs = GetComponent<Transform>();
	}
#endif

	public void Init ()
	{
		timer.onFinished += DestroyMe;
		timer.Reset ();
		timer.Start ();
		enabled = true;
	}

	public override void DoUpdate ()
	{
		image.fillAmount = timer.timeRemaining / timer.duration;
	}

	void DestroyMe (params object[] args)
	{
		timer.onFinished -= DestroyMe;
		Destroy(gameObject);
	}
}