using UnityEngine;
using Extensions;
using System;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using GameDevJourney;
using UnityEngine.UI;

namespace InfiniteBulletHell
{
	public class ItemMenu : SingletonMonoBehaviour<ItemMenu>
	{
		public static bool isOpen;
		public Canvas canavs;
		public RectTransform canvasRectTrs;
		public Transform itemMenuEntriesParent;
		public ItemMenuEntry itemMenuEntryPrefab;

		void Awake ()
		{
			instance = this;
			gameObject.SetActive(false);
		}

		public void Open ()
		{
			GameManager.instance.PauseGame (true);
			gameObject.SetActive(true);
			isOpen = true;
		}

		public void Close ()
		{
			gameObject.SetActive(false);
			GameManager.instance.PauseGame (false);
			isOpen = false;
		}

		public void AddEntry (Item item)
		{
			ItemMenuEntry itemMenuEntry = Instantiate(itemMenuEntryPrefab, itemMenuEntriesParent);
			itemMenuEntry.SetItem (item);
		}
	}
}