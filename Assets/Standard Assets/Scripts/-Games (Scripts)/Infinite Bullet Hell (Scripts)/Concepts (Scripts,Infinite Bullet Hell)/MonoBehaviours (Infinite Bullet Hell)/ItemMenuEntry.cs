using UnityEngine;
using Extensions;
using System;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using GameDevJourney;
using UnityEngine.UI;
using TMPro;

namespace InfiniteBulletHell
{
	public class ItemMenuEntry : MonoBehaviour
	{
		public Item item;
		public Image image;
		public Button useButton;
		public TMP_Text useText;
		public Tooltip tooltip;

		public void SetItem (Item item)
		{
			this.item = item;
			image.sprite = item.spriteRenderer.sprite;
			UseableItem useableItem = item as UseableItem;
			if (useableItem != null)
			{
				useText.text = "Use";
				if (useableItem.uses > 1)
					useText.text += " (" + (useableItem.uses - useableItem.usedCount) + ")";
				useButton.onClick.RemoveAllListeners();
				useButton.onClick.AddListener(() => { useableItem.Use (); if (useableItem.usedCount == useableItem.uses) Destroy(gameObject); else SetItem (item); });
			}
			else
				useButton.gameObject.SetActive(false);
			tooltip.canvas = ItemMenu.instance.canavs;
			tooltip.canvasRectTrs = ItemMenu.instance.canvasRectTrs;
			tooltip.text.text = item.tooltip.text.text;
			tooltip.Awake ();
		}
	}
}