using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace GameDevJourney
{
	[CreateAssetMenu]
	public class ShootAtRelativeAngle : BulletPattern
	{
        public float angle;

		public override Vector2 GetShootDirection (Transform spawner)
		{
			return spawner.up.Rotate(angle);
		}
	}
}