﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace GameDevJourney
{
	[CreateAssetMenu]
	public class ShootAtAngleAndRotate : ShootWhereFacing
	{
		public float initAngle;
		public float rotate;
		Vector2 currentDir;
		
		public override void Init (Transform spawner)
		{
			currentDir = VectorExtensions.FromFacingAngle(initAngle);
		}
		
		public override Vector2 GetShootDirection (Transform spawner)
		{
			currentDir = currentDir.Rotate(rotate);
			return currentDir;
		}
	}
}