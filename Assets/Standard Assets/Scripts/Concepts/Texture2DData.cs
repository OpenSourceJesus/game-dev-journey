using UnityEngine;
using System.Collections;
using System;
using Extensions;

namespace GameDevJourney
{
	[Serializable]
	public class Texture2DData
	{
		public byte[] imageData;
		public Vector2Int size;

		public Texture2DData (byte[] imageData, Vector2Int size)
		{
			this.imageData = imageData;
			this.size = size;
		}

		public static SpriteData GetData (Sprite sprite)
		{
			return new SpriteData(ImageConversion.EncodeToPNG(sprite.texture), sprite.texture.GetSize(), sprite.pivot, sprite.pixelsPerUnit);
		}

		public void CopyTo (Texture2D texture)
		{
			texture.Resize(size.x, size.y);
			ImageConversion.LoadImage(texture, imageData);
		}

		public string ToInitializerString ()
		{
			return "new Texture2DData(" + imageData.ToInitializerString() + ", " + size.ToInitializerString() + ")";
		}
	}
}