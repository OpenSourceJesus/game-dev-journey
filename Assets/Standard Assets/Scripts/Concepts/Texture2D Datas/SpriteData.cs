using UnityEngine;
using System.Collections;
using System;
using Extensions;

namespace GameDevJourney
{
	[Serializable]
	public class SpriteData : Texture2DData
	{
        public Vector2 pivot;
        public float pixelsPerUnit;

		public SpriteData (byte[] imageData, Vector2Int size, Vector2 pivot, float pixelsPerUnit) : base (imageData, size)
		{
			this.pivot = pivot;
			this.pixelsPerUnit = pixelsPerUnit;
		}

		public static SpriteData GetData (Sprite sprite)
		{
			return new SpriteData(ImageConversion.EncodeToPNG(sprite.texture), sprite.texture.GetSize(), sprite.pivot, sprite.pixelsPerUnit);
		}

		public void CopyTo (Sprite sprite)
		{
			sprite = Sprite.Create((Texture2D) GameManager.Clone(Texture2D.whiteTexture), Rect.MinMaxRect(0, 0, size.x, size.y), pivot, pixelsPerUnit);
			sprite.texture.Resize(size.x, size.y);
			ImageConversion.LoadImage(sprite.texture, imageData);
		}

		public string ToInitializerString ()
		{
			return "new SpriteData(" + imageData.ToInitializerString() + ", " + size.ToInitializerString() + ", " + pivot.ToInitializerString() + ", " + pixelsPerUnit + ")";
		}
	}
}