// using System;
// using System.Collections.Generic;
// using System.Collections;
// using UnityEngine;
// using Random = System.Random;

// public class Test2 : MonoBehaviour
// {
// 	static Random random;

// 	void Start ()
// 	{
// 		int[] original = new int[] { 1, 4, 3, 2 };
// 		int[] desired = new int[] { 1, 2, 4, 3 };
// 		random = new Random();
// 		print(MinPieces(original, desired));
// 	}

// 	public static int MinPieces (int[] original, int[] desired)
// 	{
// 		if (original.Equals(desired))
// 			return 1;
// 		if (original.Length != desired.Length)
// 			throw new Exception("The original array is a different length than the desired array");
// 		List<>[] cutPieces = new int[original.Length][];
// 		List<int> possibleCuts = new List<int>(original.Length - 2);
// 		for (int i = 0; i < possibleCuts.Count; i ++)
// 			possibleCuts[i] = i;
// 		IEnumerable<IList> possibleCutsPermutations = GetAllPermutations(possibleCuts);
// 		for (int cutCount = 1; cutCount < original.Length - 1; cutCount ++)
// 		{
// 			foreach (IList possibleCutsPermutation in possibleCutsPermutations)
// 			{
// 				int cutAfterIndex = (int) possibleCutsPermutation[cutCount];
// 				cutPieces[cutCount] = new int[cutAfterIndex + 1];
// 				original.CopyTo(cutPieces[cutCount], 0);
// 				cutPieces[cutCount + 1] = new int[original.Length - cutAfterIndex - 1];
// 				original.CopyTo(cutPieces[cutCount + 1], cutAfterIndex);
// 			}
// 		}
// 		foreach (IList cutPiecesPermutation in GetAllPermutations(cutPieces))
// 		{
// 			print("yay");
// 			foreach (var cutPiece in cutPiecesPermutation)
// 				print(cutPiece);
// 			if (cutPiecesPermutation.Equals(desired))
// 				return cutCount + 1;
// 		}
// 		throw new Exception("The original array can't be turned into the desired array");
// 	}

// 	public static IEnumerable<IList> GetAllPermutations (IList list)
// 	{
// 		return GetPermutations(list, list.Count);
// 	}

// 	public static IEnumerable<IList> GetPermutations (IList list, int count)
// 	{
// 		if (count == 1)
// 			yield return list;
// 		else
// 		{
// 			for (int i = 0; i < count; i ++)
// 			{
// 				foreach (IList permutation in GetPermutations(list, count - 1))
// 					yield return permutation;
// 				RotateRight (list, count);
// 			}
// 		}
// 	}
	
// 	public static void RotateRight (IList list, int count)
// 	{
// 		object element = list[count - 1];
// 		list.RemoveAt(count - 1);
// 		list.Insert(0, element);
// 	}
// }