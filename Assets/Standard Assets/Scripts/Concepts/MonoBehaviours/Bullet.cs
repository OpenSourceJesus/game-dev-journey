using UnityEngine;
using Extensions;
using GameDevJourney;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	public class Bullet : Spawnable
	{
		public Rigidbody2D rigid;
		public Collider2D collider;
		[HideInInspector]
		public float initSpeed;
		public float speed;
		public SpriteRenderer spriteRenderer;
		public Color canRetargetColor;
		public Color cantRetargetColor;
		public float lifetime;
		public bool despawnOnHit;
		[HideInInspector]
		public int timesRetargeted;
		[HideInInspector]
		public bool hasFinishedRetargeting;
		[HideInInspector]
		public ObjectPool.DelayedDespawn delayedDespawn;
		
		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (spriteRenderer == null)
					spriteRenderer = GetComponent<SpriteRenderer>();
				initSpeed = speed;
				return;
			}
#endif
			rigid.velocity = trs.up * speed;
			if (lifetime > 0)
				delayedDespawn = ObjectPool.instance.DelayDespawn (prefabIndex, gameObject, trs, lifetime);
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			if (despawnOnHit)
				Despawn ();
		}

		public virtual void Despawn ()
		{
			ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
		}

		public virtual void Retarget (Vector2 direction, bool lastRetarget = false)
		{
			if (lastRetarget && hasFinishedRetargeting)
				return;
			trs.up = direction;
			rigid.velocity = trs.up * speed;
			timesRetargeted ++;
			if (lastRetarget)
			{
				hasFinishedRetargeting = true;
				spriteRenderer.color = cantRetargetColor;
				spriteRenderer.sortingOrder --;
			}
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (delayedDespawn != null && ObjectPool.instance != null)
				ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			timesRetargeted = 0;
			if (hasFinishedRetargeting)
			{
				hasFinishedRetargeting = false;
				spriteRenderer.color = canRetargetColor;
				spriteRenderer.sortingOrder ++;
			}
			StopAllCoroutines();
			speed = initSpeed;
		}
	}
}