using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectEuler10 : MonoBehaviour
{
	public int findPrimesBelowNumber;
	List<int> primes = new List<int>();

	void Start ()
	{
		for (int i = 2; i < findPrimesBelowNumber; i ++)
		{
			bool isPrime = true;
			for (int i2 = 2; i2 <= i / 2; i2 ++)
			{
				if (!primes.Contains(i2) && i / i2 == 0)
				{
					isPrime = false;
					break;
				}
			}
			if (isPrime)
			{
				primes.Add(i);
				print(i);
			}
		}
	}
}