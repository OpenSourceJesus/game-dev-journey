using UnityEngine;
using System.Collections.Generic;
using Extensions;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	public class PhysicsObject2D : MonoBehaviour
	{
		[Header("PhysicsObject2D")]
		public string layerName;
		public Collider2D collider;

#if UNITY_EDITOR
		public virtual void Awake ()
		{
			int layer = PhysicsManager2D.realLayerNames.IndexOf(layerName);
			if (layer != -1)
				gameObject.layer = layer;
		}
#endif
	}
}