using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using FastMember;
using System.Reflection;
using System.Linq;

public static class ReflectionUtilities
{
	public static T GetMember<T> (this object obj, string memberPath)
	{
		string[] memberNames = memberPath.Split('.');
		for (int i = 0; i < memberNames.Length; i ++)
		{
			string memberName = memberNames[i];
			ObjectAccessor objectAccessor = ObjectAccessor.Create(obj, true);
			obj = objectAccessor[memberName];
		}
		return (T) obj;
	}
	
	public static void SetMember<T> (this object obj, string memberPath, T value)
	{
		string[] memberNames = memberPath.Split('.');
		string memberName = null;
		object _obj = obj;
		for (int i = 0; i < memberNames.Length; i ++)
		{
			memberName = memberNames[i];
			ObjectAccessor objectAccessor = ObjectAccessor.Create(obj, true);
			_obj = obj;
			obj = objectAccessor[memberName];
		}
		TypeAccessor typeAccessor = TypeAccessor.Create(_obj.GetType(), true);
		typeAccessor[_obj, memberName] = value;
	}
	
	public static object InvokeMember<T> (this object obj, string memberPath, BindingFlags bindingFlags, params object[] args)
	{
		string[] memberNames = memberPath.Split('.');
		string memberName = null;
		object _obj = obj;
		for (int i = 0; i < memberNames.Length; i ++)
		{
			memberName = memberNames[i];
			ObjectAccessor objectAccessor = ObjectAccessor.Create(obj, true);
			_obj = obj;
			obj = objectAccessor[memberName];
		}
		return _obj.GetType().InvokeMember(memberName, bindingFlags, null, _obj, args);
	}

	public static MemberInfo[] GetAllMembers (this object obj, bool includeNonPublic, MemberTypes mask = MemberTypes.Field | MemberTypes.Property | MemberTypes.Method)
	{
		return obj.GetType().GetAllMembers(includeNonPublic, mask).ToArray();
	}

	public static MemberInfo[] GetAllMembers (this Type type, bool includeNonPublic, MemberTypes mask = MemberTypes.Field | MemberTypes.Property | MemberTypes.Method)
	{
		return type.GetAllMembers_IEnumerable(includeNonPublic, mask).ToArray();
	}

	public static IEnumerable<MemberInfo> GetAllMembers_IEnumerable (this Type type, bool includeNonPublic, MemberTypes mask = MemberTypes.Field | MemberTypes.Property | MemberTypes.Method)
	{
		const BindingFlags BINDING = BindingFlags.Public | BindingFlags.Instance;
		const BindingFlags PRIV_BINDING = BindingFlags.NonPublic | BindingFlags.Instance;
		if (type == null)
			yield break;
		foreach (MemberInfo m in type.GetMembers(BINDING))
		{
			if ((m.MemberType & mask) != 0)
				yield return m;
		}
		if (includeNonPublic)
		{
			while (type != null)
			{
				foreach (MemberInfo m in type.GetMembers(PRIV_BINDING))
				{
					if ((m.MemberType & mask) != 0)
						yield return m;
				}
				type = type.BaseType;
			}
		}
	}
}