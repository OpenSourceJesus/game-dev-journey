using System;

namespace GameDevJourney
{
	[Serializable]
	public class AnimationState
	{
		public string name;
		public int layerIndex;
		public bool isNull;
	}
}