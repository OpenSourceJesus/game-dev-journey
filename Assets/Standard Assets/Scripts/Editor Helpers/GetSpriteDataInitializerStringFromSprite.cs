#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	public class GetSpriteDataInitializerStringFromSprite : MonoBehaviour
	{
		public bool update;
		public Sprite sprite;
		public string output;

		void OnValidate ()
		{
			if (!update)
				return;
			update = false;
			Do ();
		}

		void Do ()
		{
			output = SpriteData.GetData(sprite).ToInitializerString();
		}
	}
}
#endif