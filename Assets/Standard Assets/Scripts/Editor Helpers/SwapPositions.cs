#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Extensions;

[ExecuteInEditMode]
public class SwapPositions : MonoBehaviour
{
	public static float lastSwapTime;
	public float minSwapTimeInterval = .1f;

	public virtual void Start ()
	{
		if (Time.unscaledTime - lastSwapTime < minSwapTimeInterval)
			return;
		lastSwapTime = Time.unscaledTime;
		Transform[] transforms = new Transform[0];
		Transform transform1 = Selection.transforms[0];
		Transform transform2 = Selection.transforms[1];
		Vector3 previousPosition1 = transform1.position;
		transform1.position = transform2.position;
		transform2.position = previousPosition1;
		DestroyImmediate(transform1.GetComponent<SwapPositions>());
		DestroyImmediate(transform2.GetComponent<SwapPositions>());
	}
}
#endif