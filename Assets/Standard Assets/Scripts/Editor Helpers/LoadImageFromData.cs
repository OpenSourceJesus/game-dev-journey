#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using System.IO;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	public class LoadImageFromData : MonoBehaviour
	{
		public bool update;
		public Texture2D texture;
		public Texture2DData data;

		void OnValidate ()
		{
			if (!update)
				return;
			update = false;
			Do ();
		}

		void Do ()
		{
			data.CopyTo (texture);
		}
	}
}
#endif