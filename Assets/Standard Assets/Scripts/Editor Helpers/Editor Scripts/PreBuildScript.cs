#if UNITY_EDITOR
using UnityEngine;
using System;
using UnityEditor;

namespace GameDevJourney
{
	public class PreBuildScript : EditorScript
	{
	}

	[CustomEditor(typeof(PreBuildScript))]
	public class PreBuildScriptEditor : EditorScriptEditor
	{
	}
}
#else
namespace GameDevJourney
{
	public class PreBuildScript : EditorScript
	{
	}
}
#endif