#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class SetSelectionToChildren
{
	[MenuItem("Tools/Set Selection To Children")]
	public static void Do ()
	{
		List<Object> children = new List<Object>();
		for (int i = 0; i < Selection.transforms.Length; i ++)
		{
			Transform selectedTrs = Selection.transforms[i];
			for (int i2 = 0; i2 < selectedTrs.childCount; i2 ++)
				children.Add(selectedTrs.GetChild(i2).gameObject);
		}
		Selection.objects = children.ToArray();
	}
}
#endif