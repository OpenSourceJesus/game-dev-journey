#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace GameDevJourney
{
	public class CenterTransformOnChildren : EditorScript
	{
		public Transform trs;

		public virtual void Do ()
		{
			
		}
	}

	[CustomEditor(typeof(CenterTransformOnChildren))]
	public class CenterTransformOnChildrenEditor : EditorScriptEditor
	{
	}
}
#else
namespace GameDevJourney
{
	public class CenterTransformOnChildren : EditorScript
	{
	}
}
#endif