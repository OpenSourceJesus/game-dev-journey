#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using GameDevJourney;
using Extensions;
using System;

namespace GameDevJourney
{
	public class ReplaceWithPrefabs : EditorScript
	{
		public Transform[] replace;
		public Transform prefab;
		
		public override void Do ()
		{
			foreach (Transform trs in replace)
			{
				Transform clone = PrefabUtilityExtensions.ClonePrefabInstance(prefab.gameObject).GetComponent<Transform>();
				clone.position = trs.position;
				clone.rotation = trs.rotation;
				clone.SetParent(trs.parent);
				clone.localScale = trs.localScale;
				try
				{
					DestroyImmediate(trs.gameObject);
				}
				catch (Exception)
				{
					Destroy(trs.gameObject);
				}
			}
		}
	}

	[CustomEditor(typeof(ReplaceWithPrefabs))]
	public class ReplaceWithPrefabsEditor : Editor
	{
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI ();
			EditorScript editorScript = (EditorScript) target;
			editorScript.UpdateHotkeys ();
		}
	}
}
#endif