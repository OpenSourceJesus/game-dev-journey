#if UNITY_EDITOR
using UnityEngine;

namespace GameDevJourney
{
	public class CopyLineRendererPositions : EditorScript
	{
		public LineRenderer copyFrom;
		public LineRenderer copyTo;

		public override void Do ()
		{
			Vector3[] positions = new Vector3[copyFrom.positionCount];
			copyFrom.GetPositions(positions);
			copyTo.positionCount = copyFrom.positionCount;
			copyTo.SetPositions(positions);
		}
	}
}
#endif