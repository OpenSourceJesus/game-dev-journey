#if UNITY_EDITOR
using UnityEngine.Tilemaps;
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace GameDevJourney
{
	//[ExecuteInEditMode]
	public class ClearTilemaps : EditorScript
	{
		public Transform tilemapsParent;
		List<Tilemap> tilemaps = new List<Tilemap>();

		public override void Do ()
		{
			foreach (Tilemap tilemap in tilemapsParent.GetComponentsInChildren<Tilemap>())
				tilemap.ClearAllTiles ();
		}
	}

	[CustomEditor(typeof(ClearTilemaps))]
	public class ClearTilemapsEditor : EditorScriptEditor
	{
	}
}
#endif