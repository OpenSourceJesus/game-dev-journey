#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;

namespace GameDevJourney
{
	public class ReplaceTilemapTilesWithPrefabs : EditorScript
	{
		public Tilemap tilemap;
		public Transform prefabsParent;
		public Tile tile;
		public GameObject prefab;

		public override void Do ()
		{
			foreach (Vector3Int cellPosition in tilemap.cellBounds.allPositionsWithin)
			{
				Tile _tile = tilemap.GetTile(cellPosition) as Tile;
				if (_tile != null && _tile.sprite == tile.sprite)
					Instantiate(prefab, tilemap.GetCellCenterWorld(cellPosition), default(Quaternion), prefabsParent);
			}
		}
	}

	[CustomEditor(typeof(ReplaceTilemapTilesWithPrefabs))]
	public class ReplaceTilemapTilesWithPrefabsEditor : EditorScriptEditor
	{
	}
}
#else
namespace GameDevJourney
{
	public class ReplaceTilemapTilesWithPrefabs : EditorScript
	{
	}
}
#endif