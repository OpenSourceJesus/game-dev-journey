#if UNITY_EDITOR
using UnityEngine;
using System;

namespace GameDevJourney
{
	public class ToggleExecuteInEditMode : EditorScript
	{
		public ScriptEntry[] scriptEntries = new ScriptEntry[0];

		public override void Do ()
		{
			
		}

		[Serializable]
		public class ScriptEntry
		{
			public MonoBehaviour script;
		}
	}
}
#else
namespace GameDevJourney
{
	public class ToggleExecuteInEditMode : EditorScript
	{
	}
}
#endif