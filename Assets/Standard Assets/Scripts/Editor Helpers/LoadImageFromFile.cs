#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using System.IO;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	public class LoadImageFromFile : MonoBehaviour
	{
		public bool update;
		public Texture2D texture;
		public string filePath;

		void OnValidate ()
		{
			if (!update)
				return;
			update = false;
			Do ();
		}

		void Do ()
		{
			ImageConversion.LoadImage(texture, File.ReadAllBytes(filePath));
		}
	}
}
#endif