#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using System.Reflection;
using System;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	public class GetCodeToMakeGameObjectWithComponent : MonoBehaviour
	{
		public bool update;
		public Component component;
		public string output;

		void OnValidate ()
		{
			if (!update)
				return;
			update = false;
			Do ();
		}

		void Do ()
		{
			Type componentType = component.GetType();
			output = "GameObject go = new GameObject();\n" + componentType.Name + " component = go.AddComponent<" + componentType.Name + ">();\n";
			MemberInfo[] memberInfos = componentType.GetAllMembers(false, MemberTypes.Field | MemberTypes.Property);
			for (int i = 0; i < memberInfos.Length; i ++)
			{
				MemberInfo memberInfo = memberInfos[i];
				string str = GetCodeToSetComponentMember(memberInfo);
				if (!string.IsNullOrEmpty(str))
					output += str + "\n";
			}
		}

		string GetCodeToSetComponentMember (MemberInfo memberInfo, uint forLoopVarSuffix = 1)
		{
			string output = "component." + memberInfo.Name + " = ";
			try
			{
				object value = null;
				FieldInfo fieldInfo = memberInfo as FieldInfo;
				if (fieldInfo != null)
					value = fieldInfo.GetValue(component);
				else
				{
					PropertyInfo propertyInfo = memberInfo as PropertyInfo;
					value = propertyInfo.GetValue(component);
				}
				output += ToInitializerString(value, forLoopVarSuffix);
			}
			catch (Exception e)
			{
				print(memberInfo.Name);
				print(e.Message + "\n" + e.StackTrace);
				return null;
			}
			return output;
		}

		string ToInitializerString (object obj, uint forLoopVarSuffix = 1)
		{
			string output = "";
			Type type = obj.GetType();
			if (obj == null)
				output += "null;";
			else if (type.IsValueType)
			{
				if (obj is bool)
					output += obj.ToString().ToLower() + ";";
				else
					output += obj + ";";
			}
			else
			{
				string str = obj.ToString();
				if (type.IsArray)
				{
					str = str.StartAfter(type.Namespace + ".");
					str = str.Remove(str.IndexOf("["));
					output += "new " + str + "[";
					Array array = obj as Array;
					output += "" + array.Length + "];\n";
					string forLoopVarName = "i";
					if (forLoopVarSuffix > 1)
						forLoopVarName += forLoopVarSuffix;
					output += "for (int " + forLoopVarName + " = 0; " + forLoopVarName + " < " + array.Length + "; " + forLoopVarName + " ++)\n";
					output += "{\n";
					for (int i = 0; i < array.Length; i ++)
					{
						object _obj = array.GetValue(i);
						output += ToInitializerString(_obj, forLoopVarSuffix + 1);
					}
					output += "}";
				}
				else
				{
					if (obj is string)
						output += "\"" + obj + "\";";
					else
					{
						string goName = str.Substring(0, str.IndexOf(" (" + type.Namespace + "." + type.Name));
						output += "GameObject.Find(\"" + goName + "\").GetComponent<" + type.Name + ">();";
					}
				}
			}
			return output;
		}
	}
}
#endif