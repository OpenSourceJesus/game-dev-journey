#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using System.IO;

namespace GameDevJourney
{
	[ExecuteInEditMode]
	public class CombineScriptsToFile : MonoBehaviour
	{
		public bool update;
		public TextAsset[] scripts = new TextAsset[0];
		public string filePath;

		void OnValidate ()
		{
			if (!update)
				return;
			update = false;
			Do ();
		}

		void Do ()
		{
			string fileText = "";
			List<string> usingStatements = new List<string>();
			for (int i = 0; i < scripts.Length; i ++)
			{
				TextAsset script = scripts[i];
				string[] scriptLines = script.text.Split('\n');
				for (int i2 = 0; i2 < scriptLines.Length; i2 ++)
				{
					string scriptLine = scriptLines[i2];
					if (scriptLine.StartsWith("using "))
					{
						if (!usingStatements.Contains(scriptLine))
							usingStatements.Add(scriptLine);
					}
					else
						fileText += scriptLine + "\n";
				}
			}
			for (int i = 0; i < usingStatements.Count; i ++)
			{
				string usingStatement = usingStatements[i];
				fileText = usingStatement + "\n" + fileText;
			}
			File.WriteAllText(filePath, fileText);
		}
	}
}
#endif